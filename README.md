# Get list of packages

This is a small script for getting the list of packages for automotive.

It uses the `content-resolver` to get the dependencies and a small Python
script to extract the list of packages and format it.

## Build the container

```shell
podman build -f Dockerfile -t resolver .

```

## Customize your list

There are already some config files for the `content-resolver` to work
at the directory `configs`, but you can update those files or add more files.

## Run the content-resolver from the container

```shell
podman run -it --rm -v ./configs:/configs:Z -v ./output:/output:Z resolver
```

## Print the final list of packages

```shell
./extract_package_list.py output/workload--automotive-c8s-image-manifest--automotive-c8s-package-manifest--repository-c8s--aarch64.json
```

