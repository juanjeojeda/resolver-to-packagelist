#!/usr/bin/env python3

import sys
import re
import json


def get_package_list(filename: str) -> list:
    with open(filename) as workload:
        json_data = json.load(workload)

    return json_data["data"]["pkg_added_ids"]


def clean_package_list(packages: list) -> list:
    package_list = []

    for pkg in packages:
        name_no_epoch = re.sub("-[0-9]+:", "-", pkg)
        name_no_arch = re.sub("\.(aarch64|noarch)$", "", name_no_epoch)
        package_list.append(name_no_arch)

    return package_list


def print_package_list(package_list: list):
    # Sort the list before to print it.
    for pkg in sorted(package_list):
        print(pkg)


def main():
    workload_file = sys.argv[1]

    raw_package_list = get_package_list(workload_file)

    package_list = clean_package_list(raw_package_list)

    print_package_list(package_list)


if __name__ == "__main__":
    main()
